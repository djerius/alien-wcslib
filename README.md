# NAME

Alien::WCSLIB - Build and Install the WCSLIB library

# VERSION

version 0.03

# SYNOPSIS

    use Alien::WCSLIB;

# DESCRIPTION

This module finds or builds the _WCSLIB_ library, an astronomical FITS World Coordinate System support library.

# USAGE

Please see [Alien::Build::Manual::AlienUser](https://metacpan.org/pod/Alien::Build::Manual::AlienUser) (or equivalently on [metacpan](https://metacpan.org/pod/distribution/Alien-Build/lib/Alien/Build/Manual/AlienUser.pod)).

# LICENSE

## WCSLIB

WCSLIB is distributed under the GNU Lesser General Public License, version 3 or later.

# SUPPORT

## Bugs

Please report any bugs or feature requests to bug-alien-wcslib@rt.cpan.org  or through the web interface at: https://rt.cpan.org/Public/Dist/Display.html?Name=Alien-WCSLIB

## Source

Source is available at

    https://gitlab.com/djerius/alien-wcslib

and may be cloned from

    https://gitlab.com/djerius/alien-wcslib.git

# SEE ALSO

Please see those modules/websites for more information related to this module.

- [https://www.atnf.csiro.au/people/mcalabre/WCS/](https://www.atnf.csiro.au/people/mcalabre/WCS/)

# AUTHOR

Diab Jerius <djerius@cpan.org>

# COPYRIGHT AND LICENSE

This software is Copyright (c) 2021 by Smithsonian Astrophysical Observatory.

This is free software, licensed under:

    The GNU General Public License, Version 3, June 2007
