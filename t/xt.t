#! perl

use Test2::V0;
use Test::Alien;
use Alien::WCSLIB;
use Package::Stash;

alien_ok 'Alien::WCSLIB';
my $xs = do { local $/; <DATA> };
xs_ok { xs => $xs, verbose => 1 }, with_subtest {
    my($module) = @_;
    my $stash = Package::Stash->new( $module );
    my $version = $stash->get_symbol( '&wcslib_version')->();
    diag $version;
  pass;
};

done_testing;

__DATA__

#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"
#include "wcs.h"

MODULE = TA_MODULE PACKAGE = TA_MODULE

const char*
wcslib_version( )
  CODE:
    RETVAL = wcslib_version( NULL );
  OUTPUT:
    RETVAL
