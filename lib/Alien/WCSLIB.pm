package Alien::WCSLIB;

# ABSTRACT: Build and Install the WCSLIB library

use strict;
use warnings;

our $VERSION = '0.03';

use base qw( Alien::Base );

1;

# COPYRIGHT

__END__

=pod

=for stopwords
metacpan


=head1 SYNOPSIS

  use Alien::WCSLIB;

=head1 DESCRIPTION

This module finds or builds the I<WCSLIB> library, an astronomical FITS World Coordinate System support library.

=head1 USAGE

Please see L<Alien::Build::Manual::AlienUser> (or equivalently on L<metacpan|https://metacpan.org/pod/distribution/Alien-Build/lib/Alien/Build/Manual/AlienUser.pod>).

=head1 LICENSE

=head2 WCSLIB

WCSLIB is distributed under the GNU Lesser General Public License, version 3 or later.


=head1 SEE ALSO

https://www.atnf.csiro.au/people/mcalabre/WCS/
